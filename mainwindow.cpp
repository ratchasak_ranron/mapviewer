/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mesh.h"
#include "mainwindow.h"
#include "view.h"
#include "meshdef.h"
#include "vectdef.h"

#include <QHBoxLayout>
#include <QSplitter>
#include <QFile>
#include <QDebug>
#include <QString>
#include <QtSql/QSqlField>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    loadMapDef();
    populateScene();

    h1Splitter = new QSplitter;
    h2Splitter = new QSplitter;

    QSplitter *vSplitter = new QSplitter;
    vSplitter->setOrientation(Qt::Vertical);
    vSplitter->addWidget(h1Splitter);
    vSplitter->addWidget(h2Splitter);

    View *view = new View("Map");
    view->view()->setScene(scene);
    h1Splitter->addWidget(view);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(vSplitter);
    setLayout(layout);

    setWindowTitle(tr("Map Viewer"));
}

void MainWindow::populateScene()
{
    scene = new QGraphicsScene;

    QList<MeshDef *>::iterator i = meshDefs.begin();
    while (i != meshDefs.end()) {
        QPoint minXY((*i)->mnX, (*i)->mnY);
        QPoint maxXY((*i)->mxX, (*i)->mxY);
        QGraphicsItem *item = new Mesh((*i), vectDefs);
        item->setPos(QPointF((*i)->mnX, (*i)->mnY));
        scene->addItem(item);
        ++i;
    }
}

void MainWindow::loadMapDef() {
    /* Read MeshDef File (/Defall/Meshdef.csv) */

    QString finMeshDef = ":/Defall/Meshdef.csv";
    QFile fMeshDef (finMeshDef);
    if (!fMeshDef.open(QIODevice::ReadOnly)) return;
    //file opened successfully
    fMeshDef.readLine(); // first line is not used
    while (!fMeshDef.atEnd())
    {
       QString strLine = fMeshDef.readLine();
       strLine.remove( QRegExp("\r\n") );
       QStringList lstrRow = strLine.split(",");

       MeshDef *temp = new MeshDef(lstrRow.at(0),
                                  lstrRow.at(1),
                                  lstrRow.at(2).toInt(),
                                  lstrRow.at(3).toInt(),
                                  lstrRow.at(4).toInt(),
                                  lstrRow.at(5).toInt(),
                                  lstrRow.at(6).toInt(),
                                  lstrRow.at(7).toInt());
       meshDefs.append(temp);
    }
    fMeshDef.close();

    /* Read VectDef File (/Defall/VectDef.csv) */

    QString finVectDef = ":/Defall/VectDef.csv";
    QFile fVectDef (finVectDef);
    if (!fVectDef.open(QIODevice::ReadOnly)) return;
    //file opened successfully
    fVectDef.readLine(); // first line is not used
    while (!fVectDef.atEnd())
    {
       QString strLine = fVectDef.readLine();
       strLine.remove( QRegExp("\r\n") );
       QStringList lstrRow = strLine.split(",");

       VectDef *temp = new VectDef(lstrRow.at(0).toInt(),
                                  lstrRow.at(1),
                                  lstrRow.at(2).toInt(),
                                  lstrRow.at(3).toInt(),
                                  lstrRow.at(4).toInt(),
                                  lstrRow.at(5).toInt(),
                                  lstrRow.at(6).toInt(),
                                  lstrRow.at(7).toInt(),
                                  lstrRow.at(8).toInt(),
                                  lstrRow.at(9).toInt(),
                                  lstrRow.at(10).toInt());
       vectDefs.insert(lstrRow.at(0).toInt(), temp);
    }
    fVectDef.close();
}
