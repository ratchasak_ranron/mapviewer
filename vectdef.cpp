#include "vectdef.h"

VectDef::VectDef()
{
}

VectDef::VectDef(
        int vctId,
        QString vctName,
        int cls,
        int color,
        int line,
        int initDsp,
        int initRef,
        int colSS,
        int colSE,
        int colES,
        int colEE
        )
{
    this->vctId = vctId;
    this->vctName = vctName;
    this->cls = cls;
    this->color = color;
    this->line = line;
    this->initDsp = initDsp;
    this->initRef = initRef;
    this->colSS = colSS;
    this->colSE = colSE;
    this->colES = colES;
    this->colEE = colEE;
}
