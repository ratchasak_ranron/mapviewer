#ifndef VECTDEF_H
#define VECTDEF_H

#include <QString>

class VectDef
{
public:
    VectDef();
    VectDef(
        int vctId,
        QString vctName,
        int cls,
        int color,
        int line,
        int initDsp,
        int initRef,
        int colSS,
        int colSE,
        int colES,
        int colEE);

    int vctId;
    QString vctName;
    int cls;
    int color;
    int line;
    int initDsp;
    int initRef;
    int colSS;
    int colSE;
    int colES;
    int colEE;
};

#endif // VECTDEF_H
