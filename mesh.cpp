/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "Mesh.h"

#include <QtWidgets>
#include <meshdef.h>
#include <QDebug>
#include <vadefs.h>

Mesh::Mesh(MeshDef *meshDef, QMap<int, VectDef*> vectDefs) {
    this->meshDef = meshDef;
    this->vectDefs = vectDefs;

    QString meshFile = ":/MeshData/" + this->meshDef->meshName + ".mfv";
    loadMeshVector(meshFile);

    setFlags(ItemIsSelectable | ItemIsMovable);
    setAcceptHoverEvents(true);
}

Mesh::Mesh(const QColor &color, int x, int y)
{
    this->x = x;
    this->y = y;
    this->color = color;
    setZValue((x + y) % 2);

    setFlags(ItemIsSelectable | ItemIsMovable);
    setAcceptHoverEvents(true);
}

void Mesh::loadMeshVector(QString meshFile) {
    QFile fMeshVector (meshFile);
    if (!fMeshVector.open(QIODevice::ReadOnly)) return;
    QByteArray meshData = fMeshVector.readAll();
    fMeshVector.close();

    QDataStream input(meshData);
    VectorHeader vectorHeader;
    // skip header
    input.skipRawData(432);

    while(!input.atEnd()) {
        // read vector header e.g. amountOfByte, type
        input.readRawData((char*)&vectorHeader, sizeof(VectorHeader));

        // read each point
        QVector<QPointF> vector;
        int numPoint = (vectorHeader.amountOfBytes - 20) / 4; // minus header and divide 2-byte of data
        qDebug() << meshFile << "  " << numPoint;
        for(int i=0; i<numPoint; i++) {
            Point point;
            input.readRawData((char*)&point, sizeof(Point));

            vector.append(QPointF(point.x, point.y));
            qDebug() << "(" << point.x << ", " << point.y << ")";
        }

        MeshVector meshVector(vectorHeader.type, vectorHeader.c1, vectorHeader.c2,
                              vectorHeader.ss, vectorHeader.se, vectorHeader.es, vectorHeader.ee,
                              vectorHeader.h1, vectorHeader.h2, vector);
        meshVectors.append(meshVector);
    }
}

QRectF Mesh::boundingRect() const
{
    return QRectF(0, 0, this->meshDef->mxX - this->meshDef->mnX,
                  this->meshDef->mnX - this->meshDef->mnX);
}

QPainterPath Mesh::shape() const
{
    QPainterPath path;
    path.addRect(14, 14, 82, 42);
    return path;
}

void Mesh::drawVector(QVector<QPointF> vector, QPainter *painter, QPen pen) {
    QPen p = painter->pen();
    painter->setPen(pen);
    QPainterPath path;
    path.moveTo(vector.first());
    for (int i = 1; i < vector.size(); ++i)
        path.lineTo(vector.at(i));
    painter->drawPath(path);
    painter->setPen(p);
}

void Mesh::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    QList<MeshVector>::iterator i = meshVectors.begin();
    while (i != meshVectors.end()) {
        QPen pen(Qt::red, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        int color = 0;
        VectDef *vectDef = vectDefs.value((*i).type);
        color = vectDef->color;
        switch((*i).type) {
        case 0 : // boundary
            pen.setColor(QColor(0, 0, 0));
            break;
        case 1 : // road center
            pen.setColor(QColor(250, 158, 37));
            break;
        case 2 : // road boundary
            pen.setColor(QColor(223, 218, 209));
            break;
        case 3 : // water
            pen.setColor(QColor(147, 186, 253));
            break;
        case 4 : // house
            pen.setColor(QColor(59, 144, 0));
            break;
        }
        drawVector((*i).vector, painter, pen);
        ++i;
    }
}

void Mesh::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    update();
}

void Mesh::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
    update();
}

void Mesh::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}
