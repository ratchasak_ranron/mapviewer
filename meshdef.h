#ifndef MESHDEF_H
#define MESHDEF_H

#include <QString>

class MeshDef
{
public:
    MeshDef();
    MeshDef( QString meshName,
        QString type,
        int mnX,
        int mnY,
        int mxX,
        int mxY,
        int fctXY,
        int fctZ);

    QString meshName;
    QString type;
    int mnX;
    int mnY;
    int mxX;
    int mxY;
    int fctXY;
    int fctZ;
};

#endif // MESHDEF_H
