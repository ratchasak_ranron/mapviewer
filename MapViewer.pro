RESOURCES += images.qrc \
    map.qrc
RESOURCES += map.qrc

HEADERS += mainwindow.h view.h \
    mesh.h \
    meshdef.h \
    vectdef.h \
    meshvector.h
SOURCES += main.cpp \
    mesh.cpp \
    meshdef.cpp \
    vectdef.cpp \
    meshvector.cpp
SOURCES += mainwindow.cpp view.cpp
QT += widgets
qtHaveModule(printsupport): QT += printsupport
qtHaveModule(opengl): QT += opengl

build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/graphicsview/chip
INSTALLS += target
