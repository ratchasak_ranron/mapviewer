#include "meshdef.h"

MeshDef::MeshDef()
{
}

MeshDef::MeshDef(QString meshName, QString type, qint32 mnX, qint32 mnY, qint32 mxX, qint32 mxY, qint32 fctXY, qint32 fctZ)
{
    this->meshName = meshName;
    this->type = type;
    this->mnX = mnX;
    this->mnY = mnY;
    this->mxX = mxX;
    this->mxY = mxY;
    this->fctXY = fctXY;
    this->fctZ = fctZ;
}
