#ifndef MESHVECTOR_H
#define MESHVECTOR_H

#include <QVector>
#include <QPointF>

class MeshVector
{
public:
    MeshVector(int type,
                int c1,
                int c2,
                int ss,
                int se,
                int es,
                int ee,
                int h1,
                int h2,
                QVector<QPointF> vector
               );

    int type;
    int c1;
    int c2;
    int ss;
    int se;
    int es;
    int ee;
    int h1;
    int h2;
    QVector<QPointF> vector;
};

typedef struct {
    quint16 amountOfBytes;
    qint16 type;
    qint16 c1;
    qint16 c2;
    qint16 ss;
    qint16 se;
    qint16 es;
    qint16 ee;
    qint16 h1;
    qint16 h2;
} VectorHeader;

typedef struct {
    qint16 x;
    qint16 y;
} Point;

#endif // MESHVECTOR_H
