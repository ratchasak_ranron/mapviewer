#include "meshvector.h"

MeshVector::MeshVector(
            int type,
            int c1,
            int c2,
            int ss,
            int se,
            int es,
            int ee,
            int h1,
            int h2,
            QVector<QPointF> vector
        )
{
    this->type = type;
    this->c1 = c1;
    this->c2 = c2;
    this->ss = ss;
    this->se = se;
    this->es = es;
    this->ee = ee;
    this->h1 = h1;
    this->h2 = h2;
    this->vector = vector;
}
